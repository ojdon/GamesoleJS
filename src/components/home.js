import React from "react";
  
  export default class FileDialogue extends React.Component {
    componentDidMount(){
      this.fileSelector = this.buildFileSelector();
    }

    buildFileSelector(){
        const fileSelector = document.createElement('input');
        fileSelector.setAttribute('type', 'file');
        fileSelector.setAttribute('multiple', 'multiple');
        return fileSelector;
      }
    
    handleFileSelect = (e) => {
      e.preventDefault();
      this.fileSelector.click();
    }
    
    render(){
      return <button onClick={this.handleFileSelect}>Select files</button>
    }
  }